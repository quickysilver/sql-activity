#0.Create database timetracke
CREATE DATABASE timetracker;

#1.Create the table departments: (id, name)
CREATE TABLE departments (
	id BIGINT NOT NULL,
	name VARCHAR(255)
);

#2.Alter the table to add the primary key constraint.
ALTER TABLE departments ADD CONSTRAINT PRIMARY KEY (id);

#3.Create the table statuses: (id, name)
CREATE TABLE statuses (
	id BIGINT NOT NULL,
	name VARCHAR(255)
);

#4.Alter the table to add the primary key constraint.
ALTER TABLE statuses ADD CONSTRAINT PRIMARY KEY (id);

#5.Create the table timeTypes: (id, name, type (varchar))
CREATE TABLE timeTypes (
	id BIGINT NOT NULL,
	name VARCHAR(255),
	type VARCHAR(255)
);

#6.Alter the table to add the primary key constraint.
ALTER TABLE timeTypes ADD CONSTRAINT PRIMARY KEY (id);

#7.Alter the table to change the data type of “type” to int.
ALTER TABLE timeTypes MODIFY type INT;

#8.Drop the column “type”.
ALTER TABLE timeTypes DROP COLUMN type;

#9.Create the table timeIns Id, date, time, timeTypeId (foreign key), statusId(foreign key)
CREATE TABLE timeIns (
	id BIGINT NOT NULL,
	date VARCHAR(255),
	time VARCHAR(255),
	timeTypeId BIGINT NOT NULL,
	statusId BIGINT NOT NULL,
	FOREIGN KEY (timeTypeId) REFERENCES timeTypes(id),
	FOREIGN KEY (statusId) REFERENCES statuses(id)
);

#10 Create the table employees Id, firstName, lastName, employeeId (Varchar), designation, departmentId(foreign ke

CREATE TABLE employees (
	id BIGINT NOT NULL,
	firstName VARCHAR(255),
	lastName VARCHAR(255),
	employeeId VARCHAR(255),
	designation VARCHAR(255),
	departmentId BIGINT NOT NULL,
	FOREIGN KEY (departmentId) REFERENCES departments(id)
);